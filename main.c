//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include "delay\delay.h"
#include "hd44780.h"
#include "buttmatrix.h"
#include "main.h"
#include <stdio.h>


uint16_t Buttons;

void main( void )
{
  buttmatrix_init();
  hd44780_init();
  hd44780_backlight_set(1);

  hd44780_goto_xy(0, 0);
  hd44780_printf("0123456789ABCDEF\r\n");

  while (1)
  {
    hd44780_goto_xy(0, 1);

    Buttons = buttmatrix_scan();

    for (uint8_t butt = 0; butt < 16; butt++)
    {
      int8_t Ch = (Buttons & (1 << butt)) ? '1' : '0';
      hd44780_write_data(Ch);
    }
    
    delay_ms(50);
  }
}
